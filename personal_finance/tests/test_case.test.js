const request = require("supertest");
const app = require("../app");
const {
    user_balance,
    income,
    expense
} = require("../models");

let token;
let userBalanceId;
let otherUserToken;
let incomeId;
let expenseId;

// truncate all existing data in test databases
expense.destroy({ truncate: true, restartIdentity: true });
income.destroy({ truncate: true, restartIdentity: true });
user_balance.destroy({ truncate: true, restartIdentity: true });

// signup API = /api/user_balance/signup
// test signup API
test('Should signup a new user', async () => {
    await request(app)
        .post('/api/user_balance/signup')
        .send({
            username: "curie",
            email: "curie@mail.com",
            password: "password"
        })
        .expect(201);
});

// test signup API error
test('Should not signup a new user, email already exists', async () => {
    await request(app)
        .post('/api/user_balance/signup')
        .send({
            username: "curie",
            email: "curie@mail.com",
            password: "password"
        })
        .expect(403);
});

// login API = /api/user_balance/login
// test login API
test('Should login successfully', async () => {
    const loginUser = await request(app)
        .post('/api/user_balance/login')
        .send({
            email: "curie@mail.com",
            password: "password"
        })
        .expect(200);
    userBalanceId = loginUser.body.data.id;
    token = loginUser.body.data.token;
});

// test login API failed email
test('Should not login because email does not match', async () => {
    await request(app)
        .post('/api/user_balance/login')
        .send({
            email: "jondoe@mail.com",
            password: "password"
        })
        .expect(403);
});

// test login API failed password
test('Should not login because password does not match', async () => {
    await request(app)
        .post('/api/user_balance/login')
        .send({
            email: "curie@mail.com",
            password: "passing"
        })
        .expect(403);
});

// user balance personal info API = /api/user_balance/personal_info/:user_balance_id
// test user balance personal info API
test('Should get user balance personal info for login user', async () => {
    const userBalanceInfo = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(200);
    userBalance = userBalanceInfo.body.data;
});

// test user balance personal info API unauthorize
test('Should not get user balance personal info for not login user', async () => {
    await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .send()
        .expect(401);
});

// test user balance personal info API forbidden user access
test('Should not get other user balance personal info', async () => {
    const signupOtherUser = await request(app)
        .post('/api/user_balance/signup')
        .send({
            username: "Jon Doe",
            email: "jondoe@mail.com",
            password: "passwordOther"
        });
    otherUserToken = signupOtherUser.body.data.token;
    await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${otherUserToken}`)
        .send()
        .expect(403);
});

// test user balance personal info API not found id
test('Should not get user balance personal info for not existed user balance id', async () => {
    await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId+3}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(404);
});

// add income API = /api/income/add
// test add income API and expect balance to be 1000000
test('Should add new income and expect balance to be 1000000', async () => {
    const incomeData = await request(app)
        .post('/api/income/add')
        .set('authorization', `Bearer ${token}`)
        .send({
            "desc": "Monthly income",
            "amount": 1000000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(201);
    incomeId = incomeData.body.data.id;
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(1000000);
});

// test add income API unauthorize
test('Should not add income for not login user', async () => {
    await request(app)
        .post('/api/income/add')
        .send({
            "desc": "Monthly income",
            "amount": 1000000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(401);
});

// test add income API forbidden user
test('Should not add other user new income', async () => {
    await request(app)
        .post('/api/income/add')
        .set('authorization', `Bearer ${otherUserToken}`)
        .send({
            "desc": "Monthly income",
            "amount": 1000000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(403);
});

// test add income API not found user id
test('Should not add new income for not existed user balance id', async () => {
    await request(app)
        .post('/api/income/add')
        .set('authorization', `Bearer ${token}`)
        .send({
            "desc": "Monthly income",
            "amount": 1000000,
            "date": "2022-05-01",
            "user_id": userBalanceId+3
        })
        .expect(404);
});

// get income API = /api/income/personal_all/user_id
// test get income API
test('Should get user income', async () => {
    await request(app)
        .get(`/api/income/personal_all/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(200);
});

// test get income API unauthorize
test('Should not get income for not login user', async () => {
    await request(app)
        .get(`/api/income/personal_all/${userBalanceId}`)
        .send()
        .expect(401);
});

// test get income API forbidden user
test('Should not get other user income', async () => {
    await request(app)
        .get(`/api/income/personal_all/${userBalanceId}`)
        .set('authorization', `Bearer ${otherUserToken}`)
        .send()
        .expect(403);
});

// test get income API not found user id
test('Should not get income for not existed user balance id', async () => {
    await request(app)
        .get(`/api/income/personal_all/${userBalanceId+3}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(404);
});

// add expense API = /api/expense/add
// test add expense API and expect balance to be 800000
test('Should add new expense and expect balance to be 800000', async () => {
    const expenseData = await request(app)
        .post('/api/expense/add')
        .set('authorization', `Bearer ${token}`)
        .send({
            "desc": "Monthly expense",
            "amount": 200000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(201);
    expenseId = expenseData.body.data.id;
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(800000);
});

// test add expense API unauthorize
test('Should not add expense for not login user', async () => {
    await request(app)
        .post('/api/expense/add')
        .send({
            "desc": "Monthly expense",
            "amount": 200000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(401);
});

// test add expense API forbidden user
test('Should not add other user new expense', async () => {
    await request(app)
        .post('/api/expense/add')
        .set('authorization', `Bearer ${otherUserToken}`)
        .send({
            "desc": "Monthly expense",
            "amount": 200000,
            "date": "2022-05-01",
            "user_id": userBalanceId
        })
        .expect(403);
});

// test add expense API not found user id
test('Should not add new expense for not existed user balance id', async () => {
    await request(app)
        .post('/api/expense/add')
        .set('authorization', `Bearer ${token}`)
        .send({
            "desc": "Monthly expense",
            "amount": 200000,
            "date": "2022-05-01",
            "user_id": userBalanceId+3
        })
        .expect(404);
});

// get expense API = /api/expense/personal_all/user_id
// test get expense API
test('Should get user expense', async () => {
    await request(app)
        .get(`/api/expense/personal_all/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(200);
});

// test get expense API unauthorize
test('Should not get expense for not login user', async () => {
    await request(app)
        .get(`/api/expense/personal_all/${userBalanceId}`)
        .send()
        .expect(401);
});

// test get expense API forbidden user
test('Should not get other user expense', async () => {
    await request(app)
        .get(`/api/expense/personal_all/${userBalanceId}`)
        .set('authorization', `Bearer ${otherUserToken}`)
        .send()
        .expect(403);
});

// test get expense API not found user id
test('Should not get expense for not existed user balance id', async () => {
    await request(app)
        .get(`/api/expense/personal_all/${userBalanceId+3}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(404);
});

// edit income API = /api/income/edit
// test edit income API and expect balance to be 1300000
test('Should edit income and expect balance to be 1300000', async () => {
    await request(app)
        .patch('/api/income/edit')
        .set('authorization', `Bearer ${token}`)
        .send({
            "id": incomeId,
            "desc": "Monthly income updated",
            "amount": 1500000,
            "date": "2022-05-01"
        })
        .expect(200);
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(1300000);
});

// test edit income API unauthorize
test('Should not edit income for not login user', async () => {
    await request(app)
        .patch('/api/income/edit')
        .send({
            "id": incomeId,
            "desc": "Monthly income updated",
            "amount": 1500000,
            "date": "2022-05-01"
        })
        .expect(401);
});

// test edit income API forbidden user
test('Should not edit other user income', async () => {
    await request(app)
        .patch('/api/income/edit')
        .set('authorization', `Bearer ${otherUserToken}`)
        .send({
            "id": incomeId,
            "desc": "Monthly income updated",
            "amount": 1500000,
            "date": "2022-05-01"
        })
        .expect(403);
});

// test edit income API not found income id
test('Should not edit income for not existed income id', async () => {
    await request(app)
        .patch('/api/income/edit')
        .set('authorization', `Bearer ${token}`)
        .send({
            "id": incomeId+3,
            "desc": "Monthly income updated",
            "amount": 1500000,
            "date": "2022-05-01"
        })
        .expect(404);
});

// edit expense API = /api/expense/edit
// test edit expense API and expect balance to be 1100000
test('Should edit expense and expect balance to be 1100000', async () => {
    await request(app)
        .patch('/api/expense/edit')
        .set('authorization', `Bearer ${token}`)
        .send({
            "id": expenseId,
            "desc": "Monthly expense updated",
            "amount": 400000,
            "date": "2022-05-01"
        })
        .expect(200);
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(1100000);
});

// test edit expense API unauthorize
test('Should not edit expense for not login user', async () => {
    await request(app)
        .patch('/api/expense/edit')
        .send({
            "id": expenseId,
            "desc": "Monthly expense updated",
            "amount": 400000,
            "date": "2022-05-01"
        })
        .expect(401);
});

// test edit expense API forbidden user
test('Should not edit other user expense', async () => {
    await request(app)
        .patch('/api/expense/edit')
        .set('authorization', `Bearer ${otherUserToken}`)
        .send({
            "id": expenseId,
            "desc": "Monthly expense updated",
            "amount": 400000,
            "date": "2022-05-01"
        })
        .expect(403);
});

// test edit expense API not found expense id
test('Should not edit expense for not existed expense id', async () => {
    await request(app)
        .patch('/api/expense/edit')
        .set('authorization', `Bearer ${token}`)
        .send({
            "id": expenseId+3,
            "desc": "Monthly expense updated",
            "amount": 400000,
            "date": "2022-05-01"
        })
        .expect(404);
});

// delete expense API = /api/expense/delete/:expense_id
// test delete expense API unauthorize
test('Should not delete expense for not login user', async () => {
    await request(app)
        .delete(`/api/expense/delete/${expenseId}`)
        .send()
        .expect(401);
});

// test delete expense API forbidden user
test('Should not delete other user expense', async () => {
    await request(app)
        .delete(`/api/expense/delete/${expenseId}`)
        .set('authorization', `Bearer ${otherUserToken}`)
        .send()
        .expect(403);
});

// test delete expense API and expect balance to be 1500000
test('Should delete expense and expect balance to be 1500000', async () => {
    await request(app)
        .delete(`/api/expense/delete/${expenseId}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(200);
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(1500000);
});

// test delete expense API not found expense id
test('Should not delete expense for not existed expense id', async () => {
    await request(app)
        .delete(`/api/expense/delete/${expenseId+3}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(404);
});

// delete income API = /api/income/delete/:income_id
// test delete income API unauthorize
test('Should not delete income for not login user', async () => {
    await request(app)
        .delete(`/api/income/delete/${incomeId}`)
        .send()
        .expect(401);
});

// test delete income API forbidden user
test('Should not delete other user income', async () => {
    await request(app)
        .delete(`/api/income/delete/${incomeId}`)
        .set('authorization', `Bearer ${otherUserToken}`)
        .send()
        .expect(403);
});

// test delete income API and expect balance to be 0
test('Should delete income and expect balance to be 0', async () => {
    await request(app)
        .delete(`/api/income/delete/${incomeId}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(200);
    const balanceData = await request(app)
        .get(`/api/user_balance/personal_info/${userBalanceId}`)
        .set('authorization', `Bearer ${token}`)
        .send();
    const balance = balanceData.body.data.balance;
    await expect(balance).toBe(0);
});

// test delete income API not found income id
test('Should not delete income for not existed income id', async () => {
    await request(app)
        .delete(`/api/income/delete/${incomeId+3}`)
        .set('authorization', `Bearer ${token}`)
        .send()
        .expect(404);
});