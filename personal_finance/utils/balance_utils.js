module.exports = {
    sumTotal: async (prev, recent) => {
        return prev + recent;
    },
    getBalancePercentage: async (income, expense) => {
        return parseInt(((expense / income) * 100).toFixed(1));
    },
    updateTotal: async (prev, recent, total) => {
        return (total - prev) + recent;
    },
    deleteTotal: async (prev, total) => {
        return total - prev;
    }
}