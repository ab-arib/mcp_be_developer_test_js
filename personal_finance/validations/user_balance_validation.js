const Joi = require("joi");
const {
    validateRequest
} = require("../middlewares/validate_request");

const signUpValidation = Joi.object().keys({
    username: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required()
});

const loginValidation = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required()
});

const signUpSchema = (req, res, next) => {
    validateRequest(req, res, next, signUpValidation);
}

const loginSchema = (req, res, next) => {
    validateRequest(req, res, next, loginValidation);
}

module.exports = {
    signUpSchema,
    loginSchema
}