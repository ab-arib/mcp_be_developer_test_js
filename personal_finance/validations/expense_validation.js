const Joi = require("joi");
const {
    validateRequest
} = require("../middlewares/validate_request");

const addExpenseValidation = Joi.object().keys({
    desc: Joi.string().required(),
    amount: Joi.number().required(),
    date: Joi.date().required(),
    user_id: Joi.number().required()
});

const editExpenseValidation = Joi.object().keys({
    id: Joi.number().required(),
    desc: Joi.string().required(),
    amount: Joi.number().required(),
    date: Joi.date().required()
});

const addExpenseSchema = (req, res, next) => {
    validateRequest(req, res, next, addExpenseValidation);
}

const editExpenseSchema = (req, res, next) => {
    validateRequest(req, res, next, editExpenseValidation);
}

module.exports = {
    addExpenseSchema,
    editExpenseSchema
}