const Joi = require("joi");
const {
    validateRequest
} = require("../middlewares/validate_request");

const addIncomeValidation = Joi.object().keys({
    desc: Joi.string().required(),
    amount: Joi.number().required(),
    date: Joi.date().required(),
    user_id: Joi.number().required()
});

const editIncomeValidation = Joi.object().keys({
    id: Joi.number().required(),
    desc: Joi.string().required(),
    amount: Joi.number().required(),
    date: Joi.date().required()
});

const addIncomeSchema = (req, res, next) => {
    validateRequest(req, res, next, addIncomeValidation);
}

const editIncomeSchema = (req, res, next) => {
    validateRequest(req, res, next, editIncomeValidation);
}

module.exports = {
    addIncomeSchema,
    editIncomeSchema
}