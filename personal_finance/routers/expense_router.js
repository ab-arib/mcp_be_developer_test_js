const router = require("express").Router();
const { isLogin } = require("../middlewares/authorization");
const expenseController = require("../controllers/expense_controller");
const {
    addExpenseSchema,
    editExpenseSchema
} = require("../validations/expense_validation");

router.get("/personal_all/:user_id", isLogin, expenseController.getAllPersonalExpense);
router.post("/add", isLogin, addExpenseSchema, expenseController.add);
router.patch("/edit", isLogin, editExpenseSchema, expenseController.edit);
router.delete("/delete/:id", isLogin, expenseController.delete);

module.exports = router;