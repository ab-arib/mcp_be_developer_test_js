const router = require("express").Router();
const userBalanceRoute = require("./user_balance_router");
const incomeRoute = require("./income_router");
const expenseRoute = require("./expense_router");

router.use("/user_balance", userBalanceRoute);
router.use("/income", incomeRoute);
router.use("/expense", expenseRoute);

module.exports = router;