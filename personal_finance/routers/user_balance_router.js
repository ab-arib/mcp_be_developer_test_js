const router = require("express").Router();
const {
    signUpSchema,
    loginSchema
} = require("../validations/user_balance_validation");
const { isLogin } = require("../middlewares/authorization");
const userBalanceController = require("../controllers/user_balance_controller");

router.post("/signup", signUpSchema, userBalanceController.signUp);
router.post("/login", loginSchema, userBalanceController.login);
router.get("/personal_info/:id", isLogin, userBalanceController.getPersonalBalanceInfoById);

module.exports = router;