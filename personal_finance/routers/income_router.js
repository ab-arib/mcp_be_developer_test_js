const router = require("express").Router();
const { isLogin } = require("../middlewares/authorization");
const incomeController = require("../controllers/income_controller");
const {
    addIncomeSchema,
    editIncomeSchema
} = require("../validations/income_validation");

router.get("/personal_all/:user_id", isLogin, incomeController.getAllPersonalIncome);
router.post("/add", isLogin, addIncomeSchema, incomeController.add);
router.patch("/edit", isLogin, editIncomeSchema, incomeController.edit);
router.delete("/delete/:id", isLogin, incomeController.delete);

module.exports = router;