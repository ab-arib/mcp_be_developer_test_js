# PERSONAL FINANCE API

Personal Finance API is a nodejs web service for monitoring income, expense, and financial balance.

## Getting Started

First, run this command to install all package dependencies:

```bash
npm install
```
Then, run this command to start the API's server:

```bash
npm run dev

or

npm run start
```

All available API requests and examples of their input and output can be seen in the **postman collection**. Furthermore, all API requests require environment variables for the values ​​`base_url` and `token`

## Authorization

All API requests require **authorization** using a **bearer token**. If not available in the postman environment variable, then you have to login using **login API** or add a new user using **signup API**.

The following is the available user data that can be used to login:

```bash
1
email: jondoe@mail.com
password: password

2
email:dave@mail.com
password: password
```
## Responses

Generally, all API requests will send JSON output like this:

```bash
{
    "status": string,
    "statusCode": integer,
    "message": string,
    "data": {object} or [array]
}
```
The `status` attributes contain a single word to represent the API response whether "success" or "failed".

The `statusCode` attributes contain an HTTP status code.

The `message` attributes contain a short message that represents the API response in general.

The `data` attribute contains the output data returned by the API response.

The following is the list of HTTP status code :

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 201 | `CREATED` |
| 400 | `BAD REQUEST` |
| 401 | `UNAUTHORIZED` |
| 403 | `FORBIDDEN` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |

## Tests
Run this command to run the test case:

```bash
npm run test
```
