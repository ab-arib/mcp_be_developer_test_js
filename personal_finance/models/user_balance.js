'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_balance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_balance.hasMany(models.income, {
        as: 'income',
        foreignKey: 'user_id'
      });
      user_balance.hasMany(models.expense, {
        as: 'expense',
        foreignKey: 'user_id'
      });
    }
  }
  user_balance.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    total_income: DataTypes.INTEGER,
    total_expense: DataTypes.INTEGER,
    balance: DataTypes.INTEGER,
    create_by: DataTypes.STRING,
    update_by: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_balance',
    tableName: 'user_balance',
    createdAt: 'created_at',
    updatedAt: 'update_at'
  });
  return user_balance;
};