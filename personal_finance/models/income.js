'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class income extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      income.belongsTo(models.user_balance, {
        as: "user_balance",
        foreignKey: "user_id"
      });
    }
  }
  income.init({
    desc: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    date: DataTypes.DATEONLY,
    user_id: DataTypes.INTEGER,
    create_by: DataTypes.STRING,
    update_by: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'income',
    tableName: 'income',
    createdAt: 'created_at',
    updatedAt: 'update_at'
  });
  return income;
};