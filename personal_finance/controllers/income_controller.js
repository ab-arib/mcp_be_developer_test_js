const { sequelize } = require("../models");
const incomeService = require("../services/income_service");
const userBalanceService = require("../services/user_balance_service");
const {
    sumTotal,
    getBalancePercentage,
    updateTotal,
    deleteTotal
} = require("../utils/balance_utils");

const incomeController = {}

incomeController.getAllPersonalIncome = async (req, res, next) => {
    try {
        console.log(`Get all personal income by user id, request = ${req.params.user_id}`);
        // check the request query
        const order = await req.query.sorBy ? req.query.sorBy : 'id';
        const direction = await req.query.direction ? req.query.direction : 'ASC';
        const limit = await req.query.limit ? req.query.limit : 1000;
        const offset = await req.query.offset ? req.query.offset : 0;
        // validate if req.params.user_id exists
        const balance = await userBalanceService.getById(req.params.user_id);
        if (!balance) {
            console.log(`Get all personal income by user id failed`);
            return res.status(404).json({
                status: "failed",
                statusCode: 404,
                message: `Balance info by id = ${req.params.user_id} not found`,
                data: [],
            });
        }
        // check for authorize owner
        if (balance.email != req.user.email) {
            console.log(`Get all personal income by user id failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "Forbidden user",
                data: {},
            });
        }
        // get data
        const incomes = await incomeService.getAllByUserId(req.params.user_id, order, direction, limit, offset);
        console.log(`Get all personal income by user id success`);
        return res.status(200).json({
            status: "success",
            statusCode: 200,
            message: `Get all personal income by user id success`,
            data: incomes
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

incomeController.add = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Add income, request = ${JSON.stringify(req.body)}`);
            // get balance data by id
            const balance = await userBalanceService.getById(req.body.user_id);
            if (!balance) {
                console.log(`Add income failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Balance info by id = ${req.body.user_id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (balance.email != req.user.email) {
                console.log(`Add income failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // create new income
            const income = await incomeService.addTransactional(req.body, req.user.username, t);
            // calculate new total_income and balance
            const newTotalIncome = await sumTotal(balance.total_income, income.amount);
            // const newBalance = await getBalancePercentage(newTotalIncome, balance.total_expense); // balance = expense percentage
            const newBalance = await deleteTotal(balance.total_expense, newTotalIncome);
            // update user_balance
            await userBalanceService.updateTotalIncomeBalanceTransactional(
                req.body.user_id, newTotalIncome, newBalance, req.user.username, t
            );
            console.log(`Add income success`);
            return res.status(201).json({
                status: "success",
                statusCode: 201,
                message: `Add income success`,
                data: income
            });
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

incomeController.edit = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Edit income, request = ${JSON.stringify(req.body)}`);
            // get income data
            const income = await incomeService.getIncomeById(req.body.id);
            if (!income) {
                console.log(`Edit income failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Income by id = ${req.body.id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (income.user_balance.email != req.user.email) {
                console.log(`Edit income failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // update income
            await incomeService.updateTransactional(req.body, req.user.username, t);
            // calculate new total income and balance
            const newTotalIncome = await updateTotal(income.amount, req.body.amount, income.user_balance.total_income);
            // const newBalance = await getBalancePercentage(newTotalIncome, income.user_balance.total_expense);
            const newBalance = await deleteTotal(income.user_balance.total_expense, newTotalIncome);
            // update user_balance
            await userBalanceService.updateTotalIncomeBalanceTransactional(
                income.user_balance.id, newTotalIncome, newBalance, req.user.username, t
            );
            console.log(`Edit income success`);
            return res.status(200).json({
                status: "success",
                statusCode: 200,
                message: `Edit income success`,
                data: {}
            });
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

incomeController.delete = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Delete income by id, request = ${req.params.id}`);
            // get income data
            const income = await incomeService.getIncomeById(req.params.id);
            if (!income) {
                console.log(`Edit income failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Income by id = ${req.params.id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (income.user_balance.email != req.user.email) {
                console.log(`Edit income failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // delete income
            await incomeService.deleteTransactional(req.params.id, t);
            // calculate new total income and balance
            const newTotalIncome = await deleteTotal(income.amount, income.user_balance.total_income);
            // const newBalance = await getBalancePercentage(newTotalIncome, income.user_balance.total_expense);
            const newBalance = await deleteTotal(income.user_balance.total_expense, newTotalIncome);
            // update user_balance
            await userBalanceService.updateTotalIncomeBalanceTransactional(
                income.user_balance.id, newTotalIncome, newBalance, req.user.username, t
            );
            console.log(`Delete income success`);
            return res.status(200).json({
                status: "success",
                statusCode: 200,
                message: `Delete income success`,
                data: {}
            });
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

module.exports = incomeController;