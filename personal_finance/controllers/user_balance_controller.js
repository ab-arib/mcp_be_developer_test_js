const userBalanceService = require("../services/user_balance_service");

const userBalanceController = {}

userBalanceController.signUp = async (req, res, next) => {
    try {
        console.log(`Sign up new user, request = ${JSON.stringify(req.body)}`);
        // validate for unique username and email
        const validateUser = await userBalanceService.getUserByEmailOrName(req.body);
        if (validateUser) {
            console.log(`Sign up new user failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "username or email already used",
                data: {},
            });
        }
        // create new user
        const newUser = await userBalanceService.addNewUser(req.body);
        console.log(`Sign up new user success`);
        return res.status(201).json({
            status: "success",
            statusCode: 201,
            message: `Sign up new user success`,
            data: newUser
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

userBalanceController.login = async (req, res, next) => {
    try {
        console.log(`Login user, request = ${JSON.stringify(req.body)}`);
        // validate email user
        const user = await userBalanceService.getUserByEmail(req.body);
        if (!user) {
            console.log(`Login user failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "email or password are not valid",
                data: {},
            });
        }
        // validate password
        const loginUser = await userBalanceService.login(req.body, user);
        if (!loginUser) {
            console.log(`Login user failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "email or password are not valid",
                data: {},
            });
        }
        console.log(`Login user success`);
        return res.status(200).json({
            status: "success",
            statusCode: 200,
            message: `Login user success`,
            data: loginUser
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

userBalanceController.getPersonalBalanceInfoById = async (req, res, next) => {
    try {
        console.log(`Get personal balance info by id, request = ${req.params.id}`);
        let balance = await userBalanceService.getById(req.params.id);
        if (!balance) {
            console.log(`Get personal balance info by id failed`);
            return res.status(404).json({
                status: "failed",
                statusCode: 404,
                message: `Balance info by id = ${req.params.id} not found`,
                data: {},
            });
        }
        // check for authorize owner
        if (balance.email != req.user.email) {
            console.log(`Get personal balance info by id failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "Forbidden user",
                data: {},
            });
        }
        console.log(`Get personal balance info by id success`);
        return res.status(200).json({
            status: "success",
            statusCode: 200,
            message: `Get personal balance info by id success`,
            data: balance
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

module.exports = userBalanceController;