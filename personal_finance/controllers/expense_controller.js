const { sequelize } = require("../models");
const expenseService = require("../services/expense_service");
const userBalanceService = require("../services/user_balance_service");
const {
    sumTotal,
    getBalancePercentage,
    updateTotal,
    deleteTotal
} = require("../utils/balance_utils");

const expenseController = {}

expenseController.getAllPersonalExpense = async (req, res, next) => {
    try {
        console.log(`Get all personal expense by user id, request = ${req.params.user_id}`);
        // check the request query
        const order = await req.query.sorBy ? req.query.sorBy : 'id';
        const direction = await req.query.direction ? req.query.direction : 'ASC';
        const limit = await req.query.limit ? req.query.limit : 1000;
        const offset = await req.query.offset ? req.query.offset : 0;
        // validate if req.params.user_id exists
        const balance = await userBalanceService.getById(req.params.user_id);
        if (!balance) {
            console.log(`Get all personal expense by user id failed`);
            return res.status(404).json({
                status: "failed",
                statusCode: 404,
                message: `Balance info by id = ${req.params.user_id} not found`,
                data: [],
            });
        }
        // check for authorize owner
        if (balance.email != req.user.email) {
            console.log(`Get all personal expense by user id failed`);
            return res.status(403).json({
                status: "failed",
                statusCode: 403,
                message: "Forbidden user",
                data: {},
            });
        }
        // get data
        const expenses = await expenseService.getAllByUserId(req.params.user_id, order, direction, limit, offset);
        console.log(`Get all personal expense by user id success`);
        return res.status(200).json({
            status: "success",
            statusCode: 200,
            message: `Get all personal expense by user id success`,
            data: expenses
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

expenseController.add = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Add expense, request = ${JSON.stringify(req.body)}`);
            // get balance data by id
            const balance = await userBalanceService.getById(req.body.user_id);
            if (!balance) {
                console.log(`Add expense failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Balance info by id = ${req.body.user_id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (balance.email != req.user.email) {
                console.log(`Add expense failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // create new expense
            const expense = await expenseService.addTransactional(req.body, req.user.username, t);
            // calculate new total_expense and balance
            const newTotalExpense = await sumTotal(balance.total_expense, expense.amount);
            // const newBalance = await getBalancePercentage(balance.total_income, newTotalExpense);
            const newBalance = await deleteTotal(newTotalExpense, balance.total_income);
            // update user_balance
            await userBalanceService.updateTotalExpenseBalanceTransactional(
                req.body.user_id, newTotalExpense, newBalance, req.user.username, t
            );
            console.log(`Add expense success`);
            return res.status(201).json({
                status: "success",
                statusCode: 201,
                message: `Add expense success`,
                data: expense
            });
        })
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

expenseController.edit = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Edit expense, request = ${JSON.stringify(req.body)}`);
            // get expense data
            const expense = await expenseService.getExpenseById(req.body.id);
            if (!expense) {
                console.log(`Edit expense failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Expense by id = ${req.body.id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (expense.user_balance.email != req.user.email) {
                console.log(`Edit expense failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // update expense
            await expenseService.updateTransactional(req.body, req.user.username, t);
            // calculate new total expense and balance
            const newTotalExpense = await updateTotal(expense.amount, req.body.amount, expense.user_balance.total_expense);
            // const newBalance = await getBalancePercentage(expense.user_balance.total_income, newTotalExpense);
            const newBalance = await deleteTotal(newTotalExpense, expense.user_balance.total_income);
            // update user_balance
            await userBalanceService.updateTotalExpenseBalanceTransactional(
                expense.user_balance.id, newTotalExpense, newBalance, req.user.username, t
            );
            console.log(`Edit expense success`);
            return res.status(200).json({
                status: "success",
                statusCode: 200,
                message: `Edit expense success`,
                data: {}
            });
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

expenseController.delete = async (req, res, next) => {
    try {
        const result = await sequelize.transaction(async(t) => {
            console.log(`Delete expense, request = ${req.params.id}`);
            // get expense data
            const expense = await expenseService.getExpenseById(req.params.id);
            if (!expense) {
                console.log(`Delete expense failed`);
                return res.status(404).json({
                    status: "failed",
                    statusCode: 404,
                    message: `Expense by id = ${req.params.id} not found`,
                    data: {},
                });
            }
            // check for authorize owner
            if (expense.user_balance.email != req.user.email) {
                console.log(`Edit expense failed`);
                return res.status(403).json({
                    status: "failed",
                    statusCode: 403,
                    message: "Forbidden user",
                    data: {},
                });
            }
            // delete expense
            await expenseService.deleteTransactional(req.params.id, t);
            // calculate new total expense and balance
            const newTotalExpense = await deleteTotal(expense.amount, expense.user_balance.total_expense);
            // const newBalance = await getBalancePercentage(expense.user_balance.total_income, newTotalExpense);
            const newBalance = await deleteTotal(newTotalExpense, expense.user_balance.total_income);
            // update user_balance
            await userBalanceService.updateTotalExpenseBalanceTransactional(
                expense.user_balance.id, newTotalExpense, newBalance, req.user.username, t
            );
            console.log(`Delete expense success`);
            return res.status(200).json({
                status: "success",
                statusCode: 200,
                message: `Delete expense success`,
                data: {}
            });
        });
    } catch (e) {
        console.log(`Internal server error, e = ${e}`);
        return res.status(500).json({
            status: "failed",
            statusCode: 500,
            message: "Internal server error",
            error: e,
        });
    }
}

module.exports = expenseController;