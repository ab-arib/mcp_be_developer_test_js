const jwt = require("jsonwebtoken");

module.exports = {
    isLogin: async (req, res, next) => {
        try {
            // fetch token from header authorization
            const token = req.headers.authorization.split(" ")[1];
            if (!token) {
                return res.status(401).json({
                    status: "failed",
                    statusCode: 401,
                    message: "Unauthorized, please login",
                    error: e,
                });
            }
            // get payload data
            const payload = jwt.verify(token, "secretword");
            req.user = payload;
            if (req.user) {
                next();
            } else {
                throw new Error();
            }
        } catch (e) {
            return res.status(401).json({
                status: "failed",
                statusCode: 401,
                message: "Unauthorized, please login",
                error: e,
            });
        }
    }
}