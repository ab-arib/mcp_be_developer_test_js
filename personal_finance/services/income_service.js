const {
    income,
    user_balance
} = require("../models");

const incomeService = {}

incomeService.getAllByUserId = async (userId, order, direction, limit, offset) => {
    console.log(`Get all income by user id = ${userId}, sort by = ${order} ${direction}, limit = ${limit}, offset = ${offset}`);
    const incomes = await income.findAll({
        order: [[order,direction]],
        limit: Number(limit),
        offset: Number(offset),
        where: {
            user_id: userId
        }
    });
    return incomes;
}

incomeService.addTransactional = async (request, username, t) => {
    console.log(`add new income transactional`);
    const incomeData = await income.create({
        desc: request.desc,
        amount: request.amount,
        date: request.date,
        user_id: request.user_id,
        create_by: username
    }, { transaction: t });
    return incomeData;
}

incomeService.getIncomeById = async (incomeId) => {
    console.log(`Get income by Id`);
    const incomeData = await income.findOne({
        where: {
            id: incomeId
        },
        include: {
            model: user_balance,
            as: 'user_balance',
            attributes: {exclude: ['password']}
        }
    });
    return incomeData;
}

incomeService.updateTransactional = async (request, username, t) => {
    console.log(`update income transactional`);
    const incomeData = await income.update({
        desc: request.desc,
        amount: request.amount,
        date: request.date,
        update_by: username
    }, {
        where: {
            id: request.id
        },
        transaction: t 
    });
    return incomeData;
}

incomeService.deleteTransactional = async (incomeId, t) => {
    console.log(`delete income transactional`);
    const incomeData = await income.destroy({
        where: {
            id: incomeId
        }
    },{ transaction: t });
    return incomeData;
}

module.exports = incomeService;