const { Op } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {
    user_balance
} = require("../models");

const userBalanceService = {};

userBalanceService.getUserByEmailOrName = async (request) => {
    console.log(`Get user balance by email or username`);
    const userBalance = await user_balance.findOne({
        where: {
            [Op.or]: [
                {username: request.username},
                {email: request.email}
            ]
        }
    });
    return userBalance;
}

userBalanceService.getUserByEmail = async (request) => {
    console.log(`Get user balance by email`);
    const userBalance = await user_balance.findOne({
        where: {
            email: request.email
        }
    });
    return userBalance;
}

userBalanceService.addNewUser = async (request) => {
    console.log(`Add new user`);
    // encrypt the password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(request.password, salt);
    // save new user
    const user = await user_balance.create({
        username: request.username,
        email: request.email,
        password: hashPassword,
        total_income: 0,
        total_expense: 0,
        balance: 0,
        create_by: request.username
    });
    // construct auth payload
    const payload = {
        id: user.id,
        username: user.username,
        email: user.email
    }
    // generate jwt token
    const token = jwt.sign(payload, "secretword");
    return {
        id: user.id,
        username: user.username,
        email: user.email,
        token: token
    };
}

userBalanceService.login = async (request, user) => {
    console.log(`login user balance`);
    // check if password is match
    const comparePassword = await bcrypt.compare(request.password, user.password);
    if (!comparePassword) {
        return null;
    }
    // construct auth payload
    const payload = {
        id: user.id,
        username: user.username,
        email: user.email
    }
    // generate jwt token
    const token = jwt.sign(payload, "secretword");
    return {
        id: user.id,
        username: user.username,
        email: user.email,
        token: token
    };
} 

userBalanceService.getById = async (userBalanceId) => {
    console.log(`Get user balance by id`);
    const userBalance = await user_balance.findOne({
        attributes: {exclude: ['password']},
        where: {
            id: userBalanceId
        }
    });
    return userBalance;
}

userBalanceService.updateTotalIncomeBalanceTransactional = async (userBalanceId, totalIncome, newBalance, username, t) => {
    console.log(`Update total income and balance transactional`);
    const balance = await user_balance.update({
        total_income: totalIncome,
        balance: newBalance,
        update_by: username
    }, {
        where: {
            id: userBalanceId
        },
        transaction: t
    });
    return balance;
}

userBalanceService.updateTotalExpenseBalanceTransactional = async (userBalanceId, totalExpense, newBalance, username, t) => {
    console.log(`Update total expense and balance transactional`);
    const balance = await user_balance.update({
        total_expense: totalExpense,
        balance: newBalance,
        update_by: username
    }, {
        where: {
            id: userBalanceId
        },
        transaction: t
    });
    return balance;
}

module.exports = userBalanceService;