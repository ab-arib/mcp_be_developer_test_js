const {
    expense,
    user_balance
} = require("../models");

const expenseService = {}

expenseService.getAllByUserId = async (userId, order, direction, limit, offset) => {
    console.log(`Get all expense by user id = ${userId}, sort by = ${order} ${direction}, limit = ${limit}, offset = ${offset}`);
    const expenses = await expense.findAll({
        order: [[order,direction]],
        limit: Number(limit),
        offset: Number(offset),
        where: {
            user_id: userId
        }
    });
    return expenses;
}

expenseService.addTransactional = async (request, username, t) => {
    console.log(`add new expense transactional`);
    const expenseData = await expense.create({
        desc: request.desc,
        amount: request.amount,
        date: request.date,
        user_id: request.user_id,
        create_by: username
    }, { transaction: t });
    return expenseData;
}

expenseService.getExpenseById = async (expenseId) => {
    console.log(`Get expense by Id`);
    const expenseData = await expense.findOne({
        where: {
            id: expenseId
        },
        include: {
            model: user_balance,
            as: 'user_balance',
            attributes: {exclude: ['password']}
        }
    });
    return expenseData;
}

expenseService.updateTransactional = async (request, username, t) => {
    console.log(`update expense transactional`);
    const expenseData = await expense.update({
        desc: request.desc,
        amount: request.amount,
        date: request.date,
        update_by: username
    }, {
        where: {
            id: request.id
        },
        transaction: t 
    });
    return expenseData;
}

expenseService.deleteTransactional = async (expenseId, t) => {
    console.log(`delete expense transactional`);
    const expenseData = await expense.destroy({
        where: {
            id: expenseId
        }
    },{ transaction: t });
    return expenseData;
}

module.exports = expenseService;