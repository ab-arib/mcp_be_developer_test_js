const input = 
`2
5 3 4 6 7 8 9 1 2
6 7 2 1 9 5 3 4 8
1 9 8 3 4 2 5 6 7
8 5 9 7 6 1 4 2 3
4 2 6 8 5 3 7 9 1
7 1 3 9 2 4 8 5 6
9 6 1 5 3 7 2 8 4
2 8 7 4 1 9 6 3 5
3 4 5 2 8 6 1 7 9
2 8 6 9 4 5 1 7 3
7 1 4 6 3 2 9 5 8
9 3 5 7 8 1 4 2 6
4 2 7 3 5 6 8 1 9
6 5 8 1 9 7 3 4 2
1 9 3 4 2 8 7 6 5
3 6 1 5 7 9 2 8 4
5 4 2 8 1 3 6 9 7
8 7 9 2 6 4 5 3 1`;

// validate sudoku function
const validateSudoku = (arrData) => {
    let valid;
    
    let subSquareData = {
        one: {},
        two: {},
        three: {},
        four: {},
        five: {},
        six: {},
        seven: {},
        eight: {},
        nine: {}
    };

    for (let i = 0; i < arrData.length; i++) {
        let rowData = {};
        let columnData = {};
        for (let j = 0; j < arrData.length; j++) {
            // console.log(`i = ${i} x = ${arrData[i][j]}`,`j = ${j} y = ${arrData[j][i]}`);
            // check if value greater than 9 or lower than 1
            if (
                arrData[i][j] > 9 || arrData[i][j] < 1 ||
                arrData[j][i] > 9 || arrData[j][i] < 1
            ) {
                valid = 'Invalid';
                break;
            }
            // check if value are unique in row & column
            if (rowData[arrData[i][j]] || columnData[arrData[j][i]]) {
                valid = 'Invalid';
                break;
            }
            rowData[arrData[i][j]] = arrData[i][j];
            columnData[arrData[j][i]] = arrData[j][i];
            // construct first sub square and check if value are unique
            if (i >= 0 && i < 3 &&
                j >= 0 && j < 3) {
                if (subSquareData.one[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.one[arrData[i][j]] = arrData[i][j];
            }
            // construct second sub square and check if value are unique
            if (i >= 0 && i < 3 &&
                j >= 3 && j < 6) {
                if (subSquareData.two[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.two[arrData[i][j]] = arrData[i][j];
            }
            // construct third sub square and check if value are unique
            if (i >= 0 && i < 3 &&
                j >= 6 && j < 9) {
                if (subSquareData.three[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.three[arrData[i][j]] = arrData[i][j];
            }
            // construct fourth sub square and check if value are unique
            if (i >= 3 && i < 6 &&
                j >= 0 && j < 3) {
                if (subSquareData.four[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.four[arrData[i][j]] = arrData[i][j];
            }
            // construct fifth sub square and check if value are unique
            if (i >= 3 && i < 6 &&
                j >= 3 && j < 6) {
                if (subSquareData.five[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.five[arrData[i][j]] = arrData[i][j];
            }
            // construct sixth sub square and check if value are unique
            if (i >= 3 && i < 6 &&
                j >= 6 && j < 9) {
                if (subSquareData.six[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.six[arrData[i][j]] = arrData[i][j];
            }
            // construct seventh sub square and check if value are unique
            if (i >= 6 && i < 9 &&
                j >= 0 && j < 3) {
                if (subSquareData.seven[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.seven[arrData[i][j]] = arrData[i][j];
            }
            // construct eight sub square and check if value are unique
            if (i >= 6 && i < 9 &&
                j >= 3 && j < 6) {
                if (subSquareData.eight[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.eight[arrData[i][j]] = arrData[i][j];
            }
            // construct ninth sub square and check if value are unique
            if (i >= 6 && i < 9 &&
                j >= 6 && j < 9) {
                if (subSquareData.nine[arrData[i][j]]) {
                    valid = 'Invalid';
                    break;
                }
                subSquareData.nine[arrData[i][j]] = arrData[i][j];
            }
        }
        if (valid == 'Invalid') {
            break;
        }
    }
    if (valid == null) {
        valid = 'Valid';
    }
    return valid;
}

// main function
const main = (inputStr) => {
    // brakdown input string
    const inputArr = inputStr.split('\n');
    const totalTest = inputArr[0];
    let allInputArr =  inputArr.slice(1);
    // start validate sudoku function
    for (let i = 0; i < totalTest; i++) {
        // construct 9 x 9 array
        let arrData = [];
        const testArr = allInputArr.slice(0, 9);
        console.log(testArr);
        allInputArr.splice(0, 9);
        testArr.forEach(a => {
            let array = a.split(' ');
            arrData.push(array);
        });
        // run validate sudoku
        console.log(validateSudoku(arrData));
    }
}

main(input);